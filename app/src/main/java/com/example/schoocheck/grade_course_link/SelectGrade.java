package com.example.schoocheck.grade_course_link;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


import androidx.appcompat.app.AppCompatActivity;

import com.example.schoocheck.R;


public class SelectGrade extends AppCompatActivity {

    //constant stuffs~!
    Boolean b9 = false;
    Boolean b10 = false;
    Boolean b11 = false;
    Boolean b12 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_grade);

        //stuffs!
        Button buttonGR9 = findViewById(R.id.buttonGR9);
        Button buttonGR10 = findViewById(R.id.buttonGR10);
        Button buttonGR11 = findViewById(R.id.buttonGR11);
        Button buttonGR12 = findViewById(R.id.buttonGR12);
        ImageButton confirmButton = findViewById(R.id.confirm_button);





        //logic stuffs~!

        buttonGR9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b9=true;
                b10=false;
                b11=false;
                b12=false;

                if (b9 = true) {
                    buttonGR9.setTextColor(getResources().getColor(R.color.turquoise));
                    buttonGR10.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR11.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR12.setTextColor(getResources().getColor(R.color.blank_gray));
                }

            }


        });


        buttonGR10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b9=false;
                b10=true;
                b11=false;
                b12=false;

                if (b10 = true) {
                    buttonGR9.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR10.setTextColor(getResources().getColor(R.color.turquoise));
                    buttonGR11.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR12.setTextColor(getResources().getColor(R.color.blank_gray));
                }


            }
        });


        buttonGR11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b9=false;
                b10=false;
                b11=true;
                b12=false;

                if (b11 = true) {
                    buttonGR9.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR10.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR11.setTextColor(getResources().getColor(R.color.turquoise));
                    buttonGR12.setTextColor(getResources().getColor(R.color.blank_gray));
                }


            }
        });


        buttonGR12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                b9=true;
                b10=false;
                b11=false;
                b12=true;

                if (b12 = true) {
                    buttonGR9.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR10.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR11.setTextColor(getResources().getColor(R.color.blank_gray));
                    buttonGR12.setTextColor(getResources().getColor(R.color.turquoise));
                }

            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (b9 = true) {
                    //make grade 9
                    classesScreen();

                } else if (b10 = true) {
                    //make grade 10
                    classesScreen();

                } else if (b11 = true) {
                    //make grade 11
                    classesScreen();

                } else if (b12 = true) {
                    //make grade 12
                    classesScreen();

                }


            }
        });




    }

    public void classesScreen() {
        Intent intent = new Intent(this, MyCourses.class);
        startActivity(intent);

    }



}

package com.example.schoocheck;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TopicOfClassScience extends AppCompatActivity {

    public TopicOfClassScience() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_of_class_science1);


        Button infoNatural = findViewById(R.id.infoNatural);
        Button natural = findViewById(R.id.naturalScience);
        Button engineering = findViewById(R.id.engineering);
        Button medical = findViewById(R.id.medical);
        Button social = findViewById(R.id.social);
        Button humanities = findViewById(R.id.humanities);
        Button topic_go = findViewById(R.id.topic_good);


        natural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                natural.setTextColor(getResources().getColor(R.color.turquoise));
                engineering.setTextColor(getResources().getColor(R.color.blank_gray));
                medical.setTextColor(getResources().getColor(R.color.blank_gray));
                social.setTextColor(getResources().getColor(R.color.blank_gray));
                humanities.setTextColor(getResources().getColor(R.color.blank_gray));


            }
        });


        infoNatural.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




            }
        });

        //yes

        engineering.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                natural.setTextColor(getResources().getColor(R.color.blank_gray));
                engineering.setTextColor(getResources().getColor(R.color.turquoise));
                medical.setTextColor(getResources().getColor(R.color.blank_gray));
                social.setTextColor(getResources().getColor(R.color.blank_gray));
                humanities.setTextColor(getResources().getColor(R.color.blank_gray));



            }
        });

        medical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                natural.setTextColor(getResources().getColor(R.color.blank_gray));
                engineering.setTextColor(getResources().getColor(R.color.blank_gray));
                medical.setTextColor(getResources().getColor(R.color.turquoise));
                social.setTextColor(getResources().getColor(R.color.blank_gray));
                humanities.setTextColor(getResources().getColor(R.color.blank_gray));



            }
        });

        social.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                natural.setTextColor(getResources().getColor(R.color.blank_gray));
                engineering.setTextColor(getResources().getColor(R.color.blank_gray));
                medical.setTextColor(getResources().getColor(R.color.blank_gray));
                social.setTextColor(getResources().getColor(R.color.turquoise));
                humanities.setTextColor(getResources().getColor(R.color.blank_gray));



            }
        });

        humanities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                natural.setTextColor(getResources().getColor(R.color.blank_gray));
                engineering.setTextColor(getResources().getColor(R.color.blank_gray));
                medical.setTextColor(getResources().getColor(R.color.blank_gray));
                social.setTextColor(getResources().getColor(R.color.blank_gray));
                humanities.setTextColor(getResources().getColor(R.color.turquoise));



            }
        });

        topic_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent moooooove = new Intent(TopicOfClassScience.this, NameSubject.class);
                startActivity(moooooove);


            }
        });


    }
}
package com.example.schoocheck;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.schoocheck.grade_course_link.SelectGrade;

public class MainFragment extends Fragment implements View.OnClickListener {

    ImageButton btn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_main, container, false);

       //createClass = (Button) view.findViewById(R.id.moving_buutton);
       //createClass.setOnClickListener(this);
        btn = (ImageButton) view.findViewById(R.id.moving_buutton);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SelectGrade.class));

            }
        });

       return view;

    }


    @Override
    public void onClick(View view) {

        Intent intent = new Intent(getActivity(),SelectGrade.class);
        startActivity(intent);

        

    }


    public void nextScreech() {
        Intent intent = new Intent(getActivity(),SelectGrade.class);
        startActivity(intent);

    }



}

